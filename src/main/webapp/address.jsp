<%@page import="javax.servlet.jsp.tagext.TryCatchFinally"%>
<%@ page language="java" import="java.util.*"%>
<%@ page language="java" import="java.sql.*"%>
<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<head>
</head>
<body>

	<%
	String button = request.getParameter("submit");
	String type = "";
	String city = "";
	String province = "";
	String zipCode = "";
	String street = "";
	String houseNumber = "";
	String id = null;
	boolean edit = false;
	if(button != null && button.equals("Edit")){
		edit = true;
		type = request.getParameter("type");
		city = request.getParameter("city");
		province = request.getParameter("province");
		zipCode = request.getParameter("zipCode");
		street = request.getParameter("street");
		houseNumber = request.getParameter("houseNumber");
		id = request.getParameter("id");
	}
	System.out.print(province);
	%>
	<br>
	<br>
	<br>
	<form method="post">
		<table align="center" width="300px"
			style="background-color: #EDF6EA; border: 1px solid #000000;">



			<tr>
				<td colspan=2 style="font-weight: bold;" align="center">Edit
					Data</td>
			</tr>
			<tr>
				<td colspan=2 align="center" height="10px"></td>
			</tr>
			<%
			if(button != null && button.equals("Edit")){
				out.print("<tr style='display: none;'><td>Id</td><td><input type=text name=id value='" + id + "'</td></tr>");
			}
			%>
		
			<tr>
				<td>Typ</td>
				<td><select class="form-control" id="type" name="type">
                    <option <%if(type.equals("Zameldowania")) out.print("selected='selected'"); %> value="Zameldowania">Zameldowania</option>
                    <option <%if(type.equals("Korespondencyjny")) out.print("selected='selected'"); %> value="Korespondencyjny">Korespondencyjny</option>
                    <option <%if(type.equals("Pracy")) out.print("selected='selected'"); %>  value="Pracy">Pracy</option>
                </select></td>
			</tr>
			<tr>
				<td>Województwo</td>
				<td>
				<select id="province" name="province">
                    <option <%if(province.equals("Dolnoslaskie")) out.print("selected='selected'"); %> value="Dolnoslaskie">Dolnosląskie</option>
                    <option <%if(province.equals("KujawskoPomorskie")) out.print("selected='selected'"); %> value="KujawskoPomorskie">Kujawsko-Pomorskie</option>
                    <option <%if(province.equals("Lubelskie")) out.print("selected='selected'"); %> value="Lubelskie">Lubelskie</option>
                    <option <%if(province.equals("Lubuskie")) out.print("selected='selected'"); %> value="Lubuskie">Lubuskie</option>
                    <option <%if(province.equals("Lodzkie")) out.print("selected='selected'"); %> value="Łódzkie">Łódzkie</option>
                    <option <%if(province.equals("Malopolskie")) out.print("selected='selected'"); %> value="Malopolskie">Małopolskie</option>
                    <option <%if(province.equals("Mazowieckie")) out.print("selected='selected'"); %> value="Mazowieckie">Mazowieckie</option>
                    <option <%if(province.equals("Opolskie")) out.print("selected='selected'"); %> value="Opolskie">Opolskie</option>
                    <option <%if(province.equals("Podkarpackie")) out.print("selected='selected'"); %> value="Podkarpackie">Podkarpackie</option>
                    <option <%if(province.equals("Podlaskie")) out.print("selected='selected'"); %> value="Podlaskie">Podlaskie</option>
                    <option <%if(province.equals("Pomorskie")) out.print("selected='selected'"); %> value="Pomorskie">Pomorskie</option>
                    <option <%if(province.equals("Slaskie")) out.print("selected='selected'"); %> value="Slaskie">Śląskie</option>
                    <option <%if(province.equals("Swietokrzyskie")) out.print("selected='selected'"); %> value="Swietokrzyskie">Świętokrzyskie</option>
                    <option <%if(province.equals("WarminskoMazurskie")) out.print("selected='selected'"); %> value="WarminskoMazurskie">Warmińsko-Mazurskie</option>
                    <option <%if(province.equals("Wielkopolskie")) out.print("selected='selected'"); %> value="Wielkopolskie">Wielkopolskie</option>
                    <option <%if(province.equals("Zachodniopomorskie")) out.print("selected='selected'"); %> value="Zachodniopomorskie">Zachodniopomorskie</option>
                </select>
				</td>
			</tr>
			<tr>
				<td>Miasto</td>
				<td><input type="text" name="city" value="<%=city%>"></td>
			</tr>
			<tr>
				<td>Kod pocztowy</td>
				<td><input type="text" name="zipCode" value="<%=zipCode%>"></td>
			</tr>
			<tr>
				<td>Ulica</td>
				<td><input type="text" name="street" value="<%=street%>"></td>
			</tr>
			
			<tr>
				<td>Numer domu</td>
				<td><input type="text" name="house" value="<%=houseNumber%>"></td>
			</tr>
			
			<tr>

			<%
				if(button != null && button.equals("Edit")){
					out.print("<td><input type='submit' name='Submit' value='Update' style='background-color: #49743D; font-weight: bold; color: #ffffff;'	formaction='editAddress'></td>");
				}
				else{
					out.print("<td><input type='submit' name='Submit' value='Add' style='background-color: #49743D; font-weight: bold; color: #ffffff;' formaction='addAddress'></td>");
				}

				
			%>


				</td>
			</tr>
			<tr>
				<td colspan=2 align="center" height="10px"></td>
			</tr>
		</table>
	</form>



</body>
</html>