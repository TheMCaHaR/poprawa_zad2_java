import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.example.se.dao.DAO;
import com.example.se.dao.PersonBuilder;
import com.example.se.domain.Person;

@WebServlet(urlPatterns = "/modify")
public class modify extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		PrintWriter out = response.getWriter();
		String id = request.getParameter("id");
		String login = request.getParameter("login");
		String pass = request.getParameter("pass");
		String email = request.getParameter("email");
		String permision = request.getParameter("Permision");
		String button = request.getParameter("Submit");
		
		DAO db = null ;
		try {
			db = new DAO();
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		PersonBuilder personBuilder = new PersonBuilder();
		String updateStatement = null;
		String message = null;
		
		try {
			if (button.equals("Update")) {
				Person  person = personBuilder.build(login, pass, email, permision);
				db.edit(person, id);
				message = "Zmieniono dane uzytkownika ";
			} else if (button.equals("Delete")) {
				db.delete(id); 
				message = "Usunieto uzytkownika ";
			}

		} catch (NullPointerException e) {
			response.sendRedirect("admin.jsp");
		}

		
		out.print(message + login + ".");
		response.setHeader("Refresh", "2;url=admin.jsp");
		
	}
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {}
}