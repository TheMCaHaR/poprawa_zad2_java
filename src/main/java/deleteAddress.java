import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.example.se.dao.DAO;

@WebServlet(urlPatterns = "deleteAddress")
public class deleteAddress extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
       DAO db = null;
        try {
        	db = new DAO();
            db.deleteAddress(request.getParameter("id"));
        } catch (Exception e) {
            e.printStackTrace();
        }

        response.sendRedirect("addressPage.jsp");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        this.doPost(request, response);
    }
}