import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.example.se.dao.DAO;
import com.example.se.domain.Person;

@WebServlet(urlPatterns = "/login")
public class login extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		response.setContentType("text/html");

		HttpSession session = request.getSession();
		PrintWriter out = response.getWriter();

		String login = request.getParameter("login");
		String pass = request.getParameter("pass");
		
		DAO db = null;
		
		out.write("Invalid login or password");
		try {
			db = new DAO();
			db.setPrintWriter(response.getWriter());
			Person p = db.get(login, pass);
			if (p != null) {
				session.setAttribute("permision", p.getPrivilages().name());
				session.setAttribute("login", login);
				response.sendRedirect("success.jsp");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {}
}